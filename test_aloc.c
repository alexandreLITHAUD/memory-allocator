#include "mem.h"
#include "common.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

void afficher_zone(void *adresse, size_t taille, int free);

int main(int argc, char **argv) {


	fprintf(stderr, "Test réalisant de multiples possibilité de d'allocation de mémoire pour comparer les FITS .\n"
			"Définir DEBUG à la compilation pour avoir une sortie un peu plus verbeuse.\n"
 		"\n");

    /* TESTS */

    /* 1er TEST */
    // Cas ou WORST est différent
    printf("1er TEST : Cas ou WORST est différent\n");
    mem_init(get_memory_adr(), 8192);
	void* first1 = mem_alloc(2000);
    mem_alloc(2000);
    mem_free(first1);
    mem_alloc(1000);
    printf("Résultat attendu : \n");
    printf("- SI FIRST FIT : la premiere ZL sera utilisé\n");
    printf("    - ZO ZL ZO ZL\n");
    printf("- SI BEST FIT : la premiere ZL sera utilisé\n");
    printf("    - ZO ZL ZO ZL\n");
    printf("- SI WORST FIT : la dernière ZL sera utilisé\n");
    printf("    - ZL ZO ZO ZL\n\n");
    mem_show(&afficher_zone);
    printf("\n");

    /* 2eme TEST */
    // Cas ou BEST est différent
    printf("2eme TEST : Cas ou BEST est différent\n");
    mem_init(get_memory_adr(), 8192);
	void* first2 = mem_alloc(4000);
    mem_alloc(2000);
    mem_free(first2);
    mem_alloc(2000);
    printf("Résultat attendu : \n");
    printf("- SI FIRST FIT : la premiere ZL sera utilisé\n");
    printf("    - ZO ZL ZO ZL\n");
    printf("- SI BEST FIT : la deuxième ZL sera utilisé\n");
    printf("    - ZL ZO ZO ZL\n");
    printf("- SI WORST FIT : la premiere ZL sera utilisé\n");
    printf("    - ZO ZL ZO ZL\n\n");
    mem_show(&afficher_zone);
    printf("\n");

    return EXIT_SUCCESS;

}


void afficher_zone(void *adresse, size_t taille, int free)
{
  printf("Zone %s, Adresse : %lu, Taille : %lu\n", free?"libre":"occupee",
         adresse - get_memory_adr(), (unsigned long) taille);
}