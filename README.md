# Compte-Rendu - Allocateur Mémoire : *Lithaud Ferrari*

- [Compte-Rendu - Allocateur Mémoire : *Lithaud Ferrari*](#compte-rendu---allocateur-mémoire--lithaud-ferrari)
- [Les choix d'implémentations](#les-choix-dimplémentations)
- [Les fonctionnalités](#les-fonctionnalités)
- [Les limites du code](#les-limites-du-code)
- [Les extensions](#les-extensions)
- [Les tests](#les-tests)
  - [Tests d'allocation et politiques](#tests-dallocation-et-politiques)
  - [Tests de libération](#tests-de-libération)

---

**Ce compte rendu est disponible sur le dépot distant au format Markdown et pdf** 

---

# Les choix d'implémentations

- **Sur les structures** :

- Nous avons décidé dans la structure allocateur header qui définit le header général de notre mémoire de rajouter le lien du header de la première zone libre de la mémoire. Nous avons décidé de faire cela afin de pouvoir itérer sur chacune des zones libres en se basant sur un Liste Chaîné.

- Afin de vraiment pouvoir itérer sur toutes les zones libres, nous avons rajouté dans le header d'une zone libre le lien vers le header de la prochaine zone libre et NULL s'il y en a plus après. Nous avons opté pour cette architecture en Liste Simplement Chaîné pour rendre le header le plus léger possible en mémoire.

- Nous avons décidé de créer une structure ab (pour allocated block) qui représente le header d'une zone occupé qui ne contient que la taille de cette zone occupé. Nous avons décidé de faire ceci afin de facilité la lisibilité du code.

- Toutes les structures que nous avons dans notre code commencent par leur taille comme premier paramètre afin de toujours pouvoir traverser la mémoire facilement.

- **Sur les fonctions** :

- Pour la fonction mem_init([nombre]) nous avons choisie de compter dans ce nombre le header de base allocated header afin de ne pas causer des problème de débordement de mémoire.

- Nous avons choisi de pouvoir modifier quelle politique le programme va utiliser directement dans le MakeFile. Par défaut, si toutes les valeur sont à 0, la politique fit_first sera choisi.

- Dans nos fonctions, nous traitons les valeurs interdites (Alloué une taille = 0 ; si la taille est plus grande que la taille totale ou si elle ne rentre dans aucune Zone libre).

---

# Les fonctionnalités

- Notre code est capable est réaliser les fonctionnalités classique de l'allocateur c.
  - **void mem_init(void\* mem, size_t taille)**
  - **void\* mem_alloc(size_t size)**
  - **void mem_free(void \*ptr)**
  - **void mem_show(void (\*print)(void \*adr, size_t size, int free))**
  - **size_t mem_get_size(void \*zone)**

- Il est de plus capable de changer de fit pour la sélection de la zone libre (interchangeable grâce au Makefile)
  - **struct fb\* mem_fit_first(struct fb \*list, size_t size)**
  - **struct fb\* mem_fit_best(struct fb \*list, size_t size)**
  - **struct fb\* mem_fit_worst(struct fb \*list, size_t size)**

---

# Les limites du code

- Nous n'avons pas intégré de système de vérification de l'état de la mémoire. Donc nous sommes incapables de détecter des grosses erreurs comme de la corruption par exemple (à part avec mem_show).

- Certaines de nos fonctions semble particulièrement compliqué et pourrai sans aucun doute être optimisé

---

# Les extensions

Nous avons implémenté trois politiques d'allocation différentes : 
- **fit_first :** qui alloue dans la première ZL qui convient
- **fit_best :** qui alloue dans la plus petite ZL qui convient
- **fit_worst :** qui alloue dans la plus grande ZL qui convient

Nous avons décidé d'implémenter la possibilité de choisir la politique d'allocation via le Makefile pour un plus grand confort d'utilisation et de test.

---

# Les tests
## Tests d'allocation et politiques

- Le fichier **[test_aloc.c](test_aloc.c)** :

- Dans ce fichier, on teste la bonne allocation des zone mémoire selon les différents fit possible de notre code.

**TEST 1 : WORST DIFFERENT**
```
 __________________________
|  ZL  |  ZO  |     ZL     |
| 2000 | 2000 |    4000    |
|______|______|____________|
```

- Dans cette situation, on fait une allocation de 1000 (tout est en octets.):
  - Dans le cas d'une politique de fit_first ou de fit_best on doit allouer dans la première ZL (celle à 2000).
  - Dans le cas d'une politique de fit_worst on doit allouer dans la deuxième ZL (celle à 4000).

**TEST 2 : BEST DIFFERENT**
```
 __________________________
|     ZL     |  ZO  |  ZL  |
|    4000    | 2000 | 2000 |
|____________|______|______|
```

- Dans cette situation, on fait une allocation de 2000 (tout est en octets.):
  - Dans le cas d'une politique de fit_first ou de fit_worst on doit allouer dans la première ZL (celle à 4000).
  - Dans le cas d'une politique de fit_best on doit allouer dans la deuxième ZL (celle à 2000).

---

## Tests de libération

- Le fichier **[test_free.c](test_free.c)** :

- Dans ce fichier, on teste de nombreux cas de libération de mémoire dans des cas différents.

**TEST 2 : FUSION DOUBLE**
```
 ______________________
|  ZL |  ZO |    ZL    |
| 100 | 100 |   7800   |
|_____|_____|__________|
```

- Dans cette situation, on fait une libération de la ZO centrale :
  - Il ne doit rester qu'une seule ZL après cela, car elles auront toutes fusionné.


**TEST 3 : FUSION DROITE**
```
 _____________________
|  ZO |      ZL       |
| 100 |     7900      |
|_____|_______________|
```

- Dans cette situation, on libère la ZO de gauche :
  - Il ne doit rester plus qu'une seule ZL après cela, car elle aura fusionné avec la ZL de droite.