/* On inclut l'interface publique */
#include "mem.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>

/* Définition de l'alignement recherché
 * Avec gcc, on peut utiliser __BIGGEST_ALIGNMENT__
 * sinon, on utilise 16 qui conviendra aux plateformes qu'on cible
 */
#ifdef __BIGGEST_ALIGNMENT__
#define ALIGNMENT __BIGGEST_ALIGNMENT__
#else
#define ALIGNMENT 16
#endif

#ifndef FIT_FIRST
#define FIT_FIRST 0
#endif

#ifndef FIT_BEST
#define FIT_BEST 0
#endif

#ifndef FIT_WORST
#define FIT_WORST 0
#endif

/* structure placée au début de la zone de l'allocateur

   Elle contient toutes les variables globales nécessaires au
   fonctionnement de l'allocateur

   Elle peut bien évidemment être complétée
*/
struct allocator_header { //HEADER DE LA BASE DE LA MEMOIRE
    size_t memory_size;
	mem_fit_function_t *fit;
	struct fb *zone_libre; 
};

/* La seule variable globale autorisée
 * On trouve à cette adresse le début de la zone à gérer
 * (et une structure 'struct allocator_header)
 */
static void* memory_addr;

static inline void *get_system_memory_addr() {
	return memory_addr;
}

static inline struct allocator_header *get_header() {
	struct allocator_header *h;
	h = get_system_memory_addr();
	return h;
}

static inline size_t get_system_memory_size() {
	return get_header()->memory_size;
}


struct fb { // free block -> aka ZL
	size_t size;
	struct fb* next;
	/* ... */
};

// Il faut surement ajouter la taille de la zone demandé par l'utilisateur
struct ab { // allocated block -> aka ZO
	//Taille totale avec header + padding
	size_t size;
	//Taille de l'utilisateur (A Voir)
	//size_t size_user
};


void mem_init(void* mem, size_t taille)
{

	// On test si la taille est supérieur au minimum de fonctionnement
	// On test si la taille n'est pas négative (cas normalement impossible)
	// On test si la taille n'est pas supérieur à la taille max d'un int32 (choisie arbitrairement)
	if(taille < sizeof(struct allocator_header) + sizeof(struct fb))
	{
		// Dans ce cas on retourne un message d'erreur a l'utilisateur
		//printf("La taille sélectionnée n'est pas valide\n");
	}
	else 
	{
    	memory_addr = mem;
    	*(size_t*)memory_addr = taille;
		//printf("%p\n",memory_addr);
		/* On vérifie qu'on a bien enregistré les infos et qu'on
		 * sera capable de les récupérer par la suite
		 */

		assert(mem == get_system_memory_addr());
		assert(taille == get_system_memory_size());

		// On recherche la taille restante
		size_t taille_restante = taille - sizeof(struct allocator_header);

		//On recupere l'header (Il est place au bon endroit auto)
		struct allocator_header *ah = get_header();
		ah->memory_size = taille_restante;
		
		// On creer et initialise la zone libre -> Au bon endroit
		struct fb *adresse_zone_libre_base= (void*)memory_addr + sizeof(struct allocator_header);
		adresse_zone_libre_base->next = NULL;
		adresse_zone_libre_base->size = taille_restante;

		// On affecte a ah l'adresse de la zone libre
		ah->zone_libre = adresse_zone_libre_base;

		// On affecte a ah le bon fit

		if(FIT_FIRST)
			mem_fit(&mem_fit_first);
		else if(FIT_BEST)
			mem_fit(&mem_fit_best);
		else if(FIT_WORST)
			mem_fit(&mem_fit_worst);

		else
			mem_fit(&mem_fit_first);
	}
}

void mem_show(void (*print)(void *, size_t, int)) {

	// On sauvgarde l'adresse de la premiere zone libre
	struct fb* zoneLibre = get_header()->zone_libre;

	// On sauvgarde premiere adresse
	void* adresseActuelle = memory_addr + sizeof(struct allocator_header);

	// On creer un compteur
	size_t counter = 0;

	// Variable temporaire
	int free = 0; 			// Varieble qui dit si la zone est libre ou non
	size_t size = 0; 		// Variable qui va stocké la taille de la zone en cours
	struct fb* tempZL;		// Variable temporaire qui servira de cast 
	struct ab* tempZO;		// Variable temporaire qui servira de cast

	// tant que nous somme pas a la fin de la mémoire
	while (counter != get_header()->memory_size) {
		
		// si zone libre
		if(zoneLibre == adresseActuelle){
			// On change les valeurs des variables temporaires
			zoneLibre = zoneLibre->next;
			tempZL = (struct fb*)adresseActuelle;
			size = tempZL->size;
			// on définie que la zone est libre
			free = 1;
		}
		// Si zone occupé
		else{
			// On change les valeurs des variables temporaires
			tempZO = (struct ab*)adresseActuelle;
			size = tempZO->size;
			// on définie que la zone est occupé
			free = 0;
			
		}

		// on affiche grave à la fonction en paramètre
		print(adresseActuelle,size,free);

		// On met a jour le compteur et l'adresse
		counter += size;
		adresseActuelle = (void*)adresseActuelle + size;
	}
}

void mem_fit(mem_fit_function_t *f) {
	get_header()->fit = f;
}

int getTailleTotale(int taille){
	return ((taille + sizeof(struct fb)) + 7)/8*8;
}

void *mem_alloc(size_t taille) {
	
	if(taille > get_header()->memory_size || taille == 0){

		// Si l'utilisateur demande une taille supérieur la mémoire générale ou une taille null
		//printf("La taille sélectionnée n'est pas valide\n");
	}
	else 
	{

		// On recupere l'addresse de la zone libre que l'on va remplacer par une zone occupé   (On rajoute le padding à la fonction)
		struct fb *fb=get_header()->fit(get_header()->zone_libre,getTailleTotale(taille));

		// Si on ne peux pas la stocké dans aucune des ZL
		if(fb == NULL){
			return NULL;
		}

		//IF ZO FULL ZL

		// On creer la nouvelle zone libre a la "bonne" adresse
		struct fb *nouvelleZoneLibre =(void*)fb + (getTailleTotale(taille));

		// On modifie la taille de la nouvelle ZL
		nouvelleZoneLibre->size = fb->size - (getTailleTotale(taille));

		// On sauvgarde la liste finale
		nouvelleZoneLibre->next = fb->next;

		// On creer le nouveau ZO a la "bonne" adresse
		struct ab *nouvelleZoneOccupe = (struct ab*)fb;

		// On modifie les donnees de ZO
		nouvelleZoneOccupe->size = getTailleTotale(taille);

		// On met dans le header de la zone occupé la taille voulu par l'utilisateur (A Voir)
		//nouvelleZoneOccupe->size_user = taille;

		// On stocke l'adresse ou l'utilisateur va pouvoir écrire
		void* res = (void*)nouvelleZoneOccupe + sizeof(struct ab);
		
		// Si la zone libre utilisé est la permiere on mofifie la ou se trouve la première
		if(get_header()->zone_libre == fb){
			get_header()->zone_libre = nouvelleZoneLibre;
		}
		// Si la zone libre utilisé n'est pas la premiere
		else
		{
			
			// On cherche la ZL d'avant pour pouvoir lui faire pointé
			// la nouvelle valeur de ZL
			struct fb* prec;
			struct fb* temp = get_header()->zone_libre;

			do{

				if(temp == fb){
					prec->next = nouvelleZoneLibre;
					break;
				}
				prec = temp;
				temp = temp->next;

			} while(temp != NULL);
		}

		// On retourne l'adresse voulu par l'utilisateur
		return res;

	}

	// On retourne null dans les mauvais cas
	return NULL;
}


void mem_free(void* mem) {

	// On recupere l'adresse du header du ZO
	void* adresseTemp = mem - sizeof(struct ab);

	// On récupere la Zone Occupé en question
	struct ab* tempAb = (struct ab*)adresseTemp;

	// On recupere les premiere zones libres
	struct fb* premiereZoneLibre = get_header()->zone_libre;
	struct fb* next_ZoneLibre = premiereZoneLibre->next;

	// Si la première zone libre est apres a zone a libérer
	if(adresseTemp < (void*)premiereZoneLibre){

		// On créer une nouvelle zone libre
		struct fb* nouvelleZoneLibre = adresseTemp;

		// On actualise les paramètres
		nouvelleZoneLibre->next = get_header()->zone_libre;
		get_header()->zone_libre = adresseTemp;
		nouvelleZoneLibre->size = tempAb->size;

		adresseTemp = adresseTemp + nouvelleZoneLibre->size;

		// On regarde si la zone à libérer est suivie d'une zone libre
		// Et dans ce cas on les fusionnent
		if(adresseTemp == nouvelleZoneLibre->next){

			// On fusionne la ZO et la ZL
			nouvelleZoneLibre->size = nouvelleZoneLibre->size + nouvelleZoneLibre->next->size;
			nouvelleZoneLibre->next = nouvelleZoneLibre->next->next;
		}
	}

	else{

		// On cherche la ZL avant la ZO à libérer
		while((void*)next_ZoneLibre < adresseTemp){

			premiereZoneLibre = premiereZoneLibre->next;
			next_ZoneLibre = premiereZoneLibre->next;

		}

		// On définie une variable temporaire qui correspond à la première ZL avant ZO
		struct fb* zoneLibreSuivante = premiereZoneLibre;
		zoneLibreSuivante = (void*)zoneLibreSuivante + premiereZoneLibre->size;

		// Si la ZL juste avant ZO et donc sont à fusionner
		if((void*)zoneLibreSuivante == adresseTemp){ // LE PB EST ICI JE CROIE

			// On fusionne
			premiereZoneLibre->size = premiereZoneLibre->size + tempAb->size;
			zoneLibreSuivante = (void*)zoneLibreSuivante + tempAb->size;

			// On regarde si il y a une ZL à fusionner après ZO
			if((void*)zoneLibreSuivante == premiereZoneLibre->next){

				// On fusione
				premiereZoneLibre->size = premiereZoneLibre->size + premiereZoneLibre->next->size;
				premiereZoneLibre->next = premiereZoneLibre->next->next;

			}
		}

		// La ZL avant ZO n'est pas collé a ZO et donc n'est pas a fusionner
		else{

			// On transforme la ZO en ZL 
			struct fb *nouvelleZoneLibre = adresseTemp;

			// On met à jour la ZL
			nouvelleZoneLibre->size = tempAb->size;
			nouvelleZoneLibre->next = next_ZoneLibre;
			premiereZoneLibre->next = nouvelleZoneLibre->next;

			// On regarde si il y a une ZL à fusionner après ZO
			if(nouvelleZoneLibre + nouvelleZoneLibre->size == nouvelleZoneLibre->next){

				// On fusionne
				nouvelleZoneLibre->size = nouvelleZoneLibre->size + nouvelleZoneLibre->next->size;
				nouvelleZoneLibre->next = nouvelleZoneLibre->next->next;

			}
		}
		
	}

}


struct fb* mem_fit_first(struct fb *list, size_t size) {

	// On créer une variable temporaire
	struct fb* temp = list;

	// On regarde tous les ZL
	// On renvoi le premier qui est supérieur ou égal a la taille demandé
	do {

		// Si la taille est supérieur ou égale
		if(temp->size >= size){
			// On retourne l'adresse de cette zone libre
			return temp;
		}

		// Temp prend la valeur de la prochaine zone libre
		temp = temp->next;
	} while(temp != NULL);

	// Si aucune zone n'est capable de stocké
	return NULL;
}

/* Fonction à faire dans un second temps
 * - utilisée par realloc() dans malloc_stub.c
 * - nécessaire pour remplacer l'allocateur de la libc
 * - donc nécessaire pour 'make test_ls'
 * Lire malloc_stub.c pour comprendre son utilisation
 * (ou en discuter avec l'enseignant)
 */
size_t mem_get_size(void *zone) {
	/* zone est une adresse qui a été retournée par mem_alloc() */

	/* la valeur retournée doit être la taille maximale que
	 * l'utilisateur peut utiliser dans cette zone */


	// On récupere l'adresse du header de la zone
	void* temp = zone - sizeof(struct ab);

	struct ab* tempZone = (struct ab*)temp;
	//return tempZone->size_user; (A Voir)
	return tempZone->size - sizeof(struct ab);
}

/* Fonctions facultatives
 * autres stratégies d'allocation
 */
struct fb* mem_fit_best(struct fb *list, size_t size) {

	// On creer une variable temporaire
	struct fb* temp = list;

	// On creer des variables temporairess pour le résultat
	size_t min = get_header()->memory_size+1;
	struct fb* res;

	// On cherche la ZL avec la taille la plus adapté 
	while(temp != NULL){

		if(min > temp->size - size){
			min = temp->size - size;
			res = temp;
		}

		temp = temp->next;
	}

	// On verifie si la zone libre est de taille addapté
	// Potentiel NullPointerException (à vérifier)
	// Pour corriger if global
	if(res->size >= size){
		return res;
	}

	// Dans les autre cas on retourne NULL
	return NULL;
}

struct fb* mem_fit_worst(struct fb *list, size_t size) {

	// On creer une variable temporaire
	struct fb* temp = list;

	// On creer des variables temporairess pour le résultat
	size_t max = 0;
	struct fb* res;

	// On cherche la ZL avec la taille maximale 
	while(temp != NULL){

		if(max < temp->size){
			max = temp->size;
			res = temp;
		}

		temp = temp->next;

	}

	// On verifie si la plus grande zone libre est de taille addapté
	// Potentiel NullPointerException (à vérifier)
	// Pour corriger if global
	if(res->size >= size){
		return res;
	}

	// Dans les autre cas on retourne NULL
	return NULL;
}
