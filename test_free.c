#include "mem.h"
#include "common.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

void afficher_zone(void *adresse, size_t taille, int free);

int main(int argc, char **argv) {

	fprintf(stderr, "Test réalisant de multiples possibilité de libération de mémoire de l'alocateur.\n"
			"Définir DEBUG à la compilation pour avoir une sortie un peu plus verbeuse.\n"
 		"\n");

    /* TESTS */

    /* 1er TEST */
    // Cas de base
    printf("1er TEST : Cas de base\n");
    mem_init(get_memory_adr(), get_memory_size());
	void* temp = mem_alloc(100);
    mem_free(temp);

    printf("Taille attendu = %lu\n",get_memory_size()-24);
    mem_show(&afficher_zone);
    printf("\n");

    /* 2eme TEST */
    // Cas de fusion double
    printf("2eme TEST : Cas de fusion double\n");
    mem_init(get_memory_adr(), get_memory_size());
    void* first1 = mem_alloc(100);
    void* second1 = mem_alloc(100);
    mem_free(first1);
    mem_free(second1);

    printf("Taille attendu = %lu\n",get_memory_size()-24);
    mem_show(&afficher_zone);
    printf("\n");

    /* 3eme TEST */
    // Cas de fusion droite
    printf("3eme TEST : Cas de fusion droite\n");
    mem_init(get_memory_adr(), get_memory_size());
    void* first2 = mem_alloc(100);
    void* second2 = mem_alloc(100);
    mem_free(second2);
    mem_free(first2);

    printf("Taille attendu = %lu\n",get_memory_size()-24);
    mem_show(&afficher_zone);
    printf("\n");

    /* 4eme TEST */
    // Cas de fusion totale
    printf("4eme TEST : Cas de fusion totale\n");
    mem_init(get_memory_adr(), 8192);
    void* first3 = mem_alloc(8152);
    mem_free(first3);

    printf("Taille attendu = 8168\n");
    mem_show(&afficher_zone);
    printf("\n");

    /* 5eme TEST */
    // Cas de fusion gauche
    printf("5eme TEST : Cas de fusion gauche\n");
    mem_init(get_memory_adr(), 8192);
    void* first4 = mem_alloc(1);
    void* second4 = mem_alloc(8128);
    mem_free(first4);
    mem_free(second4);

    printf("Taille attendu = 8168\n");
    mem_show(&afficher_zone);
    printf("\n");


    return EXIT_SUCCESS;

}

void afficher_zone(void *adresse, size_t taille, int free)
{
  printf("Zone %s, Adresse : %lu, Taille : %lu\n", free?"libre":"occupee",
         adresse - get_memory_adr(), (unsigned long) taille);
}